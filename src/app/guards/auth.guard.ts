import {CanActivateFn, Router} from '@angular/router';
import {inject} from "@angular/core";

export const authGuard: CanActivateFn = (route, state) => {
  const router: Router = inject(Router);

  if(window.localStorage.getItem("token")){
    return true;
  } else {
    router.navigate(["/login"]);
    return false;
  }

};
