import {HttpInterceptorFn, HttpRequest} from '@angular/common/http';

export const httpInterceptor: HttpInterceptorFn = (req, next) => {

  let req2 = req.clone();

  if(window.localStorage.getItem("token")){
    req2 = req2.clone({
      setHeaders: { Authorization: "Bearer "+window.localStorage.getItem("token") }
    });
  }
  return next(req2);
};
