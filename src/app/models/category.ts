export class Category {
  id?: number;
  name?: string;
  image?: string;
  childrens?: Category[];

  constructor(id?: number, name?: string, image?: string, childrens?: Category[]) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.childrens = childrens;
  }
}
