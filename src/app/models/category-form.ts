export class CategoryForm {
  id?: number;
  name?: string;
  image?: string;
  parent?: string|CategoryForm;

  constructor(id?: number,name?: string, image?: string, parent?: string|CategoryForm) {
    this.name = name;
    this.image = image;
    this.parent = parent;
  }
}
