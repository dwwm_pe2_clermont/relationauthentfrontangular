import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {User} from "../models/user";
import {catchError, Observable, retry, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  create(user: User): Observable<User>{
    return this.httpClient.post(this.apiUrl+"/register", user).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  login(user: User){
    return this.httpClient.post<{token: string}>(this.apiUrl+'/auth', user).pipe(
      retry(1),
      catchError(this.handleLoginError)
      )
  }

  private handleLoginError(error: any){
    return throwError(error.error.message);
  }

  private handleError(error: any) {


    // Return an observable with a user-facing error message.
    return throwError(() => new ErrorEvent(error.error.detail));
  }


}
