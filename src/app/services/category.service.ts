import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {catchError, Observable, retry, throwError} from "rxjs";
import {Category} from "../models/category";
import {CategoryForm} from "../models/category-form";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  apiUrl: string = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
  }

  post(category: Category): Observable<Category> {
    return this.httpClient.post<Category>(this.apiUrl+'/categories', category).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  put(category: CategoryForm): Observable<Category>{
    return this.httpClient.put(this.apiUrl+'/categories/'+category.id, category).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getAll(): Observable<Category[]>{
    return this.httpClient.get<Category[]>(this.apiUrl+'/categories').pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  getForEdit(id: number): Observable<CategoryForm>{
    return this.httpClient.get<CategoryForm>(this.apiUrl+'/categories/edit/'+id).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  getTree(): Observable<Category[]>{
    return this.httpClient
      .get<Category[]>(this.apiUrl+'/categories/tree').pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getOne(id: number): Observable<Category> {
    return this.httpClient.get<Category>(this.apiUrl+'/categories/'+id)
      .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  delete(id: number): Observable<Category>{
    return this.httpClient.delete(this.apiUrl+"/categories/"+id).pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(() => new ErrorEvent('Something bad happened; please try again later.'));
  }

}
