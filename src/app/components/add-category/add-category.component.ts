import {Component, OnInit} from '@angular/core';
import {MatButtonModule} from "@angular/material/button";
import {Router, RouterModule} from "@angular/router";
import {Category} from "../../models/category";
import {CategoryForm} from "../../models/category-form";
import {MatProgressSpinner, MatSpinner} from "@angular/material/progress-spinner";
import {CommonModule} from "@angular/common";
import {CategoryService} from "../../services/category.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatError, MatFormFieldModule, MatLabel} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";

@Component({
  selector: 'app-add-category',
  standalone: true,
  imports: [MatButtonModule, RouterModule, MatProgressSpinner,
  CommonModule, FormsModule, MatFormFieldModule, MatInputModule,
  MatLabel, MatError, ReactiveFormsModule, MatSelectModule],
  templateUrl: './add-category.component.html',
  styleUrl: './add-category.component.css'
})
export class AddCategoryComponent implements OnInit {

  category = new CategoryForm();
  isLoading = true;
  categoriesSelect?: Category[];

  constructor(private categoryService: CategoryService, private router: Router) {
  }

  ngOnInit(): void {
    this.categoryService.getAll().subscribe(data => {
      console.log(data);
      this.categoriesSelect = data;
      this.isLoading = false;
    })
  }

  submitForm() {
    this.isLoading = true;
    this.categoryService.post(this.category).subscribe(data => {
      this.router.navigate(["/"]);
    })
  }
}
