import {Component, OnInit} from '@angular/core';
import {Router, RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {CategoryForm} from "../../models/category-form";
import {CategoryService} from "../../services/category.service";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {CommonModule} from "@angular/common";



@Component({
  selector: 'app-admin-category',
  standalone: true,
  imports: [RouterModule, MatButtonModule, MatTableModule, MatProgressSpinner,
  CommonModule],
  templateUrl: './admin-category.component.html',
  styleUrl: './admin-category.component.css'
})
export class AdminCategoryComponent implements OnInit{
  displayedColumns: string[] = ['id', 'name', 'parent', 'action'];
  dataSource: CategoryForm[] = [];
  loader = true;

  constructor(private categoryService: CategoryService, private router: Router) {
  }

  ngOnInit(): void {
    this.categoryService.getAll().subscribe(data => {
      this.dataSource = data;
      this.loader = false;
    })
  }

  logout() {
    window.localStorage.removeItem("token");
    this.router.navigate(["/"])
  }
}
