import { Component } from '@angular/core';
import {User} from "../../models/user";
import {FormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {CommonModule} from "@angular/common";
import {MatError} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule, MatButtonModule, CommonModule, MatError,
    MatInputModule, MatIconModule, MatProgressSpinner],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  user: User = new User();
  hide = true;
  isLoading = false;
  loginError?: string;

  constructor(private userService: UserService, private router: Router) {
  }

  login() {
    this.isLoading = true;
    this.userService.login(this.user).subscribe(data => {
      window.localStorage.setItem("token", data.token);
      this.router.navigate(["/admin"]);
    }, error => {
      this.loginError = error;
      this.isLoading = false;
    })
  }
}
