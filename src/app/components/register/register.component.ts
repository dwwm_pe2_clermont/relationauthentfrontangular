import { Component } from '@angular/core';
import {Router, RouterModule} from "@angular/router";
import {MatButtonModule} from "@angular/material/button";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {User} from "../../models/user";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [RouterModule, MatButtonModule, MatProgressSpinner, FormsModule,
  CommonModule, MatFormFieldModule, MatInputModule, MatIconModule],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  hide = true;
  hideConfirm = true;
  isLoading = false;
  user = new User();
  strError?: string;

  constructor(private userService: UserService, private router: Router) {
  }
  submitForm() {
    this.isLoading = true;
    this.userService.create(this.user).subscribe(data => {
      this.router.navigate(['/login']);
    },
      error => {
      alert('ok');
      console.log(error)
        this.strError = error.type;
        this.isLoading = false;

      })
  }
}
