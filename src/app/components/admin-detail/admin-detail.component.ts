import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../services/category.service";
import {ActivatedRoute, Router, RouterModule} from "@angular/router";
import {Category} from "../../models/category";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {CommonModule} from "@angular/common";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";

@Component({
  selector: 'app-admin-detail',
  standalone: true,
  imports: [MatProgressSpinner, CommonModule, RouterModule, MatButtonModule,
  MatCardModule],
  templateUrl: './admin-detail.component.html',
  styleUrl: './admin-detail.component.css'
})
export class AdminDetailComponent implements OnInit {
  isLoading = true;
  category?: Category;
  constructor(private categoryService: CategoryService,
              private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    if(id){
      let idNumber = +id;
      this.categoryService.getOne(idNumber).subscribe(data => {
        this.category = data;
        this.isLoading = false;
      })
    }
  }

  reloadData(id?: number) {
    this.router.navigate(["/admin/category/", id]);
    this.isLoading = true;
    if(id){
      let idNumber = +id;
      this.categoryService.getOne(id).subscribe(data => {
        this.category = data;
        this.isLoading = false;
      })
    }
  }

  deleteCategory(id: number) {
    this.isLoading = true;
      this.categoryService.delete(id).subscribe(data => {
        this.router.navigate(["/admin"]);
      })
  }
}
