import {Component, OnInit} from '@angular/core';
import {CommonModule} from "@angular/common";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {CategoryService} from "../../services/category.service";
import {ActivatedRoute, Router, RouterModule} from "@angular/router";
import {CategoryForm} from "../../models/category-form";
import {Category} from "../../models/category";
import {MatOption} from "@angular/material/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatError, MatFormField, MatFormFieldModule, MatLabel} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";

@Component({
  selector: 'app-edit-category',
  standalone: true,
  imports: [CommonModule, MatProgressSpinner,
    MatButtonModule, RouterModule,
    FormsModule, MatFormFieldModule, MatInputModule,
    MatLabel, MatError, ReactiveFormsModule, MatSelectModule],
  templateUrl: './edit-category.component.html',
  styleUrl: './edit-category.component.css'
})
export class EditCategoryComponent implements OnInit {
  isLoading = true;
  categoryForm = new CategoryForm();
  categoriesSelect?: Category[];

  constructor(private categoryService: CategoryService,
              private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.categoryService.getAll().subscribe(data => {
      this.categoriesSelect = data;
      if (id) {
        this.categoryService.getForEdit(+id).subscribe(dataEdit => {
          this.categoryForm = dataEdit;
          if (typeof dataEdit.parent == "object") {
            this.categoryForm.parent = '/api/categories/'+dataEdit.parent.id;
          }
          this.isLoading = false;
        })
      }
    })
  }

  submitForm() {
    this.isLoading = true;
    this.categoryService.put(this.categoryForm).subscribe(data =>{
      this.router.navigate(["/admin"]);
    })

  }

  changeSelectValue($event: any) {
   this.categoryForm.parent = $event.value;
  }
}
