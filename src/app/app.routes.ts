import { Routes } from '@angular/router';
import {AddCategoryComponent} from "./components/add-category/add-category.component";
import {TreeComponent} from "./components/tree/tree.component";
import {AdminCategoryComponent} from "./components/admin-category/admin-category.component";
import {AdminDetailComponent} from "./components/admin-detail/admin-detail.component";
import {EditCategoryComponent} from "./components/edit-category/edit-category.component";
import {RegisterComponent} from "./components/register/register.component";
import {LoginComponent} from "./components/login/login.component";
import {authGuard} from "./guards/auth.guard";

export const routes: Routes = [
  {path: '', component: TreeComponent},
  {path: 'admin/ajout', component: AddCategoryComponent, canActivate: [authGuard] },
  {path: 'admin', component: AdminCategoryComponent, canActivate:[authGuard]},
  {path: 'admin/category/:id', component: AdminDetailComponent, canActivate:[authGuard]},
  {path: 'admin/category/edit/:id', component: EditCategoryComponent, canActivate:[authGuard]},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent}
];
